﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TcModel;

namespace TcDb
{
    public class TcDbInitializer : CreateDatabaseIfNotExists<TcDbContext>
    {
        protected override void Seed(TcDbContext context)
        {
            var insertCheckProcCmd = @"ALTER PROCEDURE [dbo].[insert_check]
                                    @date [datetime2](7),
                                    @modifiedDate [datetime2](7)
                                    AS
                                    BEGIN
                                    INSERT [dbo].[TcChecks]([Date], [ModifiedDate], [Amount])
                                    VALUES (@date, @modifiedDate, 0)
    
                                    DECLARE @Id bigint
                                    SELECT @Id = [Id]
                                    FROM [dbo].[TcChecks]
                                    WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()

                                    SELECT t0.[Id], t0.[Amount], t0.[RowVersion]
                                    FROM [dbo].[TcChecks] AS t0
                                    WHERE @@ROWCOUNT > 0 AND t0.[Id] = @id
                                    END";

            context.Database.ExecuteSqlCommand(insertCheckProcCmd);

            var updateCheckProcCmd = @"ALTER PROCEDURE [dbo].[update_check]
                                    @id [bigint],
                                    @date [datetime2](7),
                                    @modifiedDate [datetime2](7),
                                    @rowVersion [rowversion]
                                    AS
                                    BEGIN
                                    DECLARE @amount money;

	                                SET @amount=(SELECT SUM(Count*Price) FROM [dbo].[TcCheckDetails] WHERE CheckId = @id)
    
                                    IF(@amount IS NULL)
                                    BEGIN
                                        SET @amount=0
                                    END

                                    UPDATE [dbo].[TcChecks]
                                    SET [Date] = @date, [ModifiedDate] = @modifiedDate, [Amount] = @amount
                                    WHERE (([Id] = @id) AND (([RowVersion] = @rowVersion) OR ([RowVersion] IS NULL AND @rowVersion IS NULL)))
    
                                    SELECT t0.[Amount], t0.[RowVersion]
                                    FROM [dbo].[TcChecks] AS t0
                                    WHERE @@ROWCOUNT > 0 AND t0.[Id] = @id
                                    END";

             context.Database.ExecuteSqlCommand(updateCheckProcCmd);

            if (!context.TaxDescriptions.Any())
            {
                var taxDescriptions = new List<TcTaxDescription>();

                string[] taxDescrs = {
                    "НДС 0%",
                    "НДС 10%",
                    "НДС 18%",
                    "Без НДС",
                    "НДС 10/110",
                    "НДС 18/118",
                };

                for (var taxDescrId = 0; taxDescrId < taxDescrs.Length; taxDescrId++)
                {
                    var item = new TcTaxDescription()
                    {
                        Description = taxDescrs[taxDescrId],
                        FiscalTaxId = taxDescrId + 1
                    };
                    taxDescriptions.Add(item);
                }

                context.TaxDescriptions.AddRange(taxDescriptions.ToArray());

            }

            if (!context.Checks.Any())
            {
                var check = new TcCheck()
                {
                    Date = DateTime.Now
                };

                check.Details.Add(new TcCheckDetail()
                {
                    Name = "Товар 1",
                    Count = 1,
                    Price = 10M,
                    TaxDescriptionId = 2
                });

                check.Details.Add(new TcCheckDetail()
                {
                    Name = "Товар 2",
                    Count = 3,
                    Price = 15M,
                    TaxDescriptionId = 3
                });

                check.Details.Add(new TcCheckDetail()
                {
                    Name = "Товар 3",
                    Count = 10,
                    Price = 10M,
                    TaxDescriptionId = 4
                });

                context.Checks.Add(check);

                context.SaveChanges();

                context.Entry(check).State = EntityState.Modified;
                context.SaveChanges();
            }

            base.Seed(context);
        }
    }
}
