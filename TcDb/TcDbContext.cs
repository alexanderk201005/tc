﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TcModel;

namespace TcDb
{
    public class TcDbContext : DbContext
    {
        public virtual DbSet<TcCheck> Checks { get; set; }
        public virtual DbSet<TcCheckDetail> CheckDetails { get; set; }
        public virtual DbSet<TcTaxDescription> TaxDescriptions { get; set; }

        public TcDbContext(string connectionString)
            : base(connectionString)
        {
            Configuration.ProxyCreationEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new TcDbInitializer());

            modelBuilder.Entity<TcCheck>()
                .MapToStoredProcedures(s =>
                    s.Insert(sp => sp.HasName("insert_check", "dbo")
                        .Parameter(pm => pm.Date, "date")
                        .Parameter(pm => pm.ModifiedDate, "modifiedDate")
                        .Result(rs => rs.Id, "id")));

            modelBuilder
                .Entity<TcCheck>()
                .MapToStoredProcedures(s =>
                    s.Update(sp => sp.HasName("update_check", "dbo")
                        .Parameter(b => b.Id, "id")
                        .Parameter(b => b.Date, "date")
                        .Parameter(b => b.ModifiedDate, "modifiedDate")
                        .Parameter(b => b.RowVersion, "rowVersion")));

            modelBuilder.Entity<TcCheck>()
                .MapToStoredProcedures(s => 
                    s.Delete(sp => sp.HasName("delete_check", "dbo")
                        .Parameter(pm => pm.Id, "id")
                        .Parameter(b => b.RowVersion, "rowVersion")));

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            var entities = ChangeTracker.Entries()
                .Where(p => p.State == EntityState.Added || p.State == EntityState.Modified)
                .Select(p => p.Entity);

            var now = DateTime.UtcNow;

            foreach (var entity in entities)
            {
                if (entity.GetType() == typeof(TcCheck))
                {
                    var i = (TcCheck)entity;
                    i.ModifiedDate = now;
                }
                if (entity.GetType() == typeof(TcCheckDetail))
                {
                    var i = (TcCheckDetail)entity;
                    i.ModifiedDate = now;
                }
            }

            try
            {
                {
                    base.SaveChanges();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return -1;
            }
        }
    }
}
