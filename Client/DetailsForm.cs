﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using Client.Code;
using Client.Properties;
using TcDb;
using TcModel;

namespace Client
{
    public partial class DetailsForm : Form
    {
        private BindingSource _bindingSource;
        private BindingSource _bindingTaxSource;

        private long _checkId;

        public DetailsForm()
        {
            InitializeComponent();

            bindingNavigatorSaveButton.Enabled = false;
        }

        public DialogResult Show(long checkId)
        {
            _checkId = checkId;

            _bindingSource = new BindingSource();
            _bindingSource.AddingNew += BindingSourceOnAddingNew;

            _bindingTaxSource = new BindingSource();

            if (!GetData())
            {
                return DialogResult.Cancel;
            }

            dataGridViewCheckDetails.Columns.Clear();

            dataGridViewCheckDetails.AutoGenerateColumns = false;
            dataGridViewCheckDetails.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCheckDetails.AllowUserToAddRows = false;

            dataGridViewCheckDetails.DataSource = _bindingSource;

            bindingNavigatorDetails.BindingSource = _bindingSource;

            bindingNavigatorSaveButton.Enabled = true;

            dataGridViewCheckDetails.Columns.Add(new DataGridViewTextBoxColumn()
            {
                Name = "Id",
                DataPropertyName = "Id",
                ReadOnly = true
            });

            dataGridViewCheckDetails.Columns.Add(new DataGridViewTextBoxColumn()
            {
                Name = "Наименование",
                DataPropertyName = "Name",
                MaxInputLength = 50
            });

            dataGridViewCheckDetails.Columns.Add(new DataGridViewTextBoxColumn()
            {
                Name = "Кол-во",
                DataPropertyName = "Count"
            });

            dataGridViewCheckDetails.Columns.Add(new DataGridViewTextBoxColumn()
            {
                Name = "Цена",
                DataPropertyName = "Price",
                DefaultCellStyle = new DataGridViewCellStyle()
                {
                    Format = "c"
                },
            });

            dataGridViewCheckDetails.Columns.Add(new DataGridViewComboBoxColumn()
            {
                Name = "НДС",
                DataPropertyName = "TaxDescriptionId",
                ValueMember = "Id",
                DisplayMember = "Description",
                DataSource = _bindingTaxSource,
                ValueType = typeof(long),
            });

            dataGridViewCheckDetails.Columns.Add(new DataGridViewTextBoxColumn()
            {
                Name = "Дата изменения",
                DataPropertyName = "ModifiedDate",
                DefaultCellStyle = new DataGridViewCellStyle()
                {
                    Format = "dd.MM.yyyy HH:mm:ss"
                },
                ReadOnly = true
            });

            dataGridViewCheckDetails.Columns.Add(new DataGridViewTextBoxColumn()
            {
                Name = "RowVersion",
                DataPropertyName = "RowVersion",
                ReadOnly = true
            });

            return ShowDialog();
        }

        private void BindingSourceOnAddingNew(object sender, AddingNewEventArgs addingNewEventArgs)
        {
            addingNewEventArgs.NewObject = new TcCheckDetail()
            {
                TaxDescriptionId = 1,
                CheckId = _checkId,
                Name = "Наименование"
            };
        }

        private void dataGridViewCheckDetails_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 6 && e.Value != null)
            {
                var array = (byte[]) e.Value;
                e.Value = "0x";
                foreach (var b in array)
                {
                    e.Value += string.Format("{0:X2}", b);
                }
            }
        }

        private void bindingNavigatorSaveButton_Click(object sender, EventArgs e)
        {
            dataGridViewCheckDetails.EndEdit();

            SaveData();

            dataGridViewCheckDetails.Refresh();
        }
       
        private void dataGridViewCheckDetails_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show(string.Format("({0},{1}): {2}", e.RowIndex, e.ColumnIndex, e.Exception.Message),
               @"Data Error",
               MessageBoxButtons.OK,
               MessageBoxIcon.Warning);

            Debug.WriteLine(string.Format("({0},{1}): {2}", e.RowIndex, e.ColumnIndex, e.Exception.Message));
        }

        private bool GetData()
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(Settings.Default.ConnectionString))
                {
                   var checkDetailsRepo = unitOfWork.GetRepository<TcCheckDetail>();
                    var taxDescriptionsRepo = unitOfWork.GetRepository<TcTaxDescription>();

                    var checkDetails = checkDetailsRepo.FindBy(cd => cd.CheckId == _checkId);

                    _bindingSource.DataSource = checkDetails.ToList();
                    _bindingTaxSource.DataSource = taxDescriptionsRepo.Entities.ToList();
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    @"Не удалось получить данные из базы данных.",
                    @"Ошибка",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

            return false;
        }

        private void SaveData()
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(Properties.Settings.Default.ConnectionString))
                {
                    var checkDetailsRepo = unitOfWork.GetRepository<TcCheckDetail>();

                    var existsCheckDetails = checkDetailsRepo.FindBy(cd => cd.CheckId == _checkId);

                    if (!(_bindingSource.DataSource is List<TcCheckDetail> datagridValues))
                    {
                        throw new ArgumentNullException("datagridValues");
                    }

                    foreach (var existingChild in existsCheckDetails.ToArray())
                    {
                        if (!datagridValues.Any(
                            u => u.Id.Equals(existingChild.Id)))
                        {
                            unitOfWork.ObjectContext.DeleteObject(existingChild);
                        }
                    }

                    foreach (var childModel in datagridValues)
                    {
                        var existingChild = existsCheckDetails
                            .Include(c=> c.TcCheck)
                            .SingleOrDefault(c => c.Id.Equals(childModel.Id));

                        if (existingChild != null)
                        {
                            existingChild.Id = childModel.Id;
                            existingChild.CheckId = childModel.CheckId;
                            existingChild.TaxDescriptionId = childModel.TaxDescriptionId;
                            existingChild.Count = childModel.Count;
                            existingChild.Price = childModel.Price;

                            unitOfWork.ObjectStateManager.GetObjectStateEntry(existingChild)
                                .ChangeState(EntityState.Modified);

                            unitOfWork.ObjectStateManager.GetObjectStateEntry(existingChild.TcCheck)
                                .ChangeState(EntityState.Modified);
                        }
                        else
                        {
                            checkDetailsRepo.Add(childModel);
                        }
                    }
                    unitOfWork.Commit();
                }
            }
            catch (DbUpdateConcurrencyException ex)
            {
                MessageBox.Show(
                    @"Объект ранее уже был изменен.",
                    @"Ошибка",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    @"Не удалось сохранить данные в базе данных.",
                    @"Ошибка",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void dataGridViewCheckDetails_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                if (string.IsNullOrEmpty(e.FormattedValue.ToString()))
                {
                    dataGridViewCheckDetails.Rows[e.RowIndex].ErrorText =
                        "Поле не может быть пустым";
                    e.Cancel = true;
                }
            }
        }

        private void dataGridViewCheckDetails_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            dataGridViewCheckDetails.Rows[e.RowIndex].ErrorText = string.Empty;
        }
    }
}
