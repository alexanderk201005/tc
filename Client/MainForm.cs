﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Client.Code;
using Client.Controls;
using Client.Properties;
using TcDb;
using TcModel;

namespace Client
{
    public partial class TcClientForm : Form
    {
        private BindingSource _bindingSource;

        public TcClientForm()
        {
            InitializeComponent();

            bindingNavigatorSaveButton.Enabled = false;
        }
        
        private void ConnectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var connectionDialog = new ConnectionDialog())
            {
                connectionDialog.StartPosition = FormStartPosition.CenterParent;

                if (connectionDialog.Show(Settings.Default.ConnectionString) == DialogResult.OK)
                {
                    Settings.Default.ConnectionString = connectionDialog.ConnectionString;
                    Settings.Default.Save();

                    ConnectToDb();
                }
            }
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ConnectToDb()
        {
            try
            {
                _bindingSource = new BindingSource();
                _bindingSource.AddingNew += BindingSourceOnAddingNew;

                if (!GetData())
                {
                    return;
                }

                dataGridViewChecks.Columns.Clear();

                dataGridViewChecks.AutoGenerateColumns = false;
                dataGridViewChecks.AllowUserToAddRows = false;

                dataGridViewChecks.Columns.Add(new DataGridViewTextBoxColumn()
                {
                    Name = "Id",
                    DataPropertyName = "Id",
                    ReadOnly = true
                });

                dataGridViewChecks.Columns.Add(new CalendarColumn()
                {
                    Name = "Дата",
                    DataPropertyName = "Date"
                });

                dataGridViewChecks.Columns.Add(new DataGridViewTextBoxColumn()
                {
                    Name = "Сумма",
                    DataPropertyName = "Amount",
                    ReadOnly = true,
                    DefaultCellStyle = new DataGridViewCellStyle()
                    {
                        Format = "c"
                    }
                });

                dataGridViewChecks.Columns.Add(new DataGridViewButtonColumn()
                {
                    Name = "Подробно",
                    Text = "Подробно...",
                    UseColumnTextForButtonValue = true
                });

                dataGridViewChecks.Columns.Add(new DataGridViewTextBoxColumn()
                {
                    Name = "Дата изменения",
                    DataPropertyName = "ModifiedDate",
                    DefaultCellStyle = new DataGridViewCellStyle()
                    {
                        Format = "dd.MM.yyyy HH:mm:ss"
                    },
                    ReadOnly = true
                });

                dataGridViewChecks.Columns.Add(new DataGridViewTextBoxColumn()
                {
                    Name = "RowVersion",
                    DataPropertyName = "RowVersion",
                    ReadOnly = true
                });

                bindingNavigatorChecks.BindingSource = _bindingSource;

                dataGridViewChecks.DataSource = _bindingSource;

                bindingNavigatorSaveButton.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    @"Не удалось подключится к базе данных.",
                    @"Ошибка",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void BindingSourceOnAddingNew(object sender, AddingNewEventArgs addingNewEventArgs)
        {
            addingNewEventArgs.NewObject = new TcCheck()
            {
                Date = DateTime.Now
            };
        }

        private void dataGridViewChecks_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                if (!string.IsNullOrEmpty(e.FormattedValue.ToString()))
                {
                    if (!DateTime.TryParse(e.FormattedValue.ToString(), out var dt))
                    {
                        MessageBox.Show(
                            @"Введите дату в правильном формате",
                            @"Ошибка",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                        e.Cancel = true;
                    }
                }
            }
        }

        private void bindingNavigatorSaveButton_Click(object sender, EventArgs e)
        {
            dataGridViewChecks.EndEdit();

            SaveData();

            GetData();

            dataGridViewChecks.Refresh();
        }

        private void dataGridViewChecks_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex != 5 || e.Value == null) return;
            var array = (byte[])e.Value;
            e.Value = "0x";
            foreach (var b in array)
            {
                e.Value += string.Format("{0:X2}", b);
            }
        }

        private void dataGridViewChecks_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                if (senderGrid.Rows[e.RowIndex].DataBoundItem != null)
                {
                    var checkId = ((TcCheck)senderGrid.Rows[e.RowIndex].DataBoundItem).Id;

                    if (checkId > 0)
                    {
                        ShowDetailsForm(checkId);
                    }
                    else
                    {
                        MessageBox.Show(
                            @"Необходимо сохранить данные.",
                            @"Ошибка",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void ShowDetailsForm(long id)
        {
            using (var detailsForm = new DetailsForm())
            {
                detailsForm.StartPosition = FormStartPosition.CenterParent;

                detailsForm.Show(id);

                GetData();

                dataGridViewChecks.Refresh();
            }
        }

        private bool GetData()
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(Settings.Default.ConnectionString))
                {
                    var checksRepo = unitOfWork.GetRepository<TcCheck>();

                    _bindingSource.DataSource = checksRepo.Entities.ToList();
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    @"Не удалось получить данные из базы данных.",
                    @"Ошибка",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

            return false;
        }

        private void SaveData()
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(Settings.Default.ConnectionString))
                {
                    var checksRepo = unitOfWork.GetRepository<TcCheck>();

                    var existsCheckDetails = checksRepo.Entities.ToList();

                    if (!(_bindingSource.DataSource is List<TcCheck> datagridValues))
                    {
                        throw new ArgumentNullException("datagridValues");
                    }

                    foreach (var existingChild in existsCheckDetails.ToArray())
                    {
                        if (!datagridValues.Any(
                            u => u.Id.Equals(existingChild.Id)))
                        {
                            unitOfWork.ObjectContext.DeleteObject(existingChild);
                        }
                    }

                    foreach (var childModel in datagridValues)
                    {
                        var existingChild = existsCheckDetails
                            .SingleOrDefault(c => c.Id.Equals(childModel.Id));

                        if (existingChild != null)
                        {
                            existingChild.Id = childModel.Id;
                            existingChild.Date = childModel.Date;

                            unitOfWork.ObjectStateManager.GetObjectStateEntry(existingChild)
                                .ChangeState(EntityState.Modified);
                        }
                        else
                        {
                            checksRepo.Add(childModel);
                        }
                    }
                    unitOfWork.Commit();
                }

            }
            catch (DbUpdateConcurrencyException ex)
            {
                MessageBox.Show(
                    @"Объект ранее уже был изменен.",
                    @"Ошибка",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    @"Не удалось сохранить данные в базе данных.",
                    @"Ошибка",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
    }
}
