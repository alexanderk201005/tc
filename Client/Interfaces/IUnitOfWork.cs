﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Interfaces
{
    public interface IUnitOfWork
    {
        DbContext DbContext { get; }
        ObjectContext ObjectContext { get; }
        ObjectStateManager ObjectStateManager { get; }

        IRepository<T> GetRepository<T>() where T : class;

        void Commit();

        void Dispose();
    }
}
