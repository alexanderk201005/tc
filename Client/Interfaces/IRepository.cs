﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> Entities { get; }

        IQueryable<T> FindBy(System.Linq.Expressions.Expression<Func<T, bool>> predicate);

        void Remove(T entity);
        void Add(T entity);
        void Update(T entity, bool submitImmediately = false);

        void RejectChanges();

        void Attach(T entity);
    }
}
