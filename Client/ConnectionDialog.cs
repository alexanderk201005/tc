﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class ConnectionDialog : Form
    {
        public string ConnectionString => textBoxConnection.Text;

        public ConnectionDialog()
        {
            InitializeComponent();
        }

        public DialogResult Show(string connectionString)
        {
            textBoxConnection.Text = connectionString;
            return ShowDialog();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
