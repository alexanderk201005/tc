﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Interfaces;
using TcDb;

namespace Client.Code
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly DbContext _dbContext;

        private bool _disposed;

        public DbContext DbContext => _dbContext;

        public ObjectContext ObjectContext
            => ((IObjectContextAdapter)_dbContext).ObjectContext;

        public ObjectStateManager ObjectStateManager
            => ((IObjectContextAdapter)_dbContext).ObjectContext.ObjectStateManager;

        public UnitOfWork(string connectionString) : this(new TcDbContext(connectionString))
        {
        }

        public UnitOfWork(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IRepository<T> GetRepository<T>() where T : class
        {
            return new GenericRepository<T>(_dbContext);
        }

        public void Commit()
        {
            _dbContext.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (_disposed) return;

            if (disposing)
            {
                _dbContext?.Dispose();
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
