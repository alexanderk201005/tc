﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcModel
{
    public class TcCheck
    {
        [Key]
        public long Id { get; set; }

        [DisplayName("Дата")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm:ss}")]
        [Column(TypeName = "datetime2")]
        public DateTime Date { get; set; }

        [DisplayName("Сумма")]
        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public decimal Amount { get; private set; }
        
        public virtual ICollection<TcCheckDetail> Details { get; set; }

        [DisplayName("Дата изменения")]
        [Column(TypeName = "datetime2")]
        public DateTime ModifiedDate { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        public TcCheck()
        {
            Details = new HashSet<TcCheckDetail>();
        }
    }
}
