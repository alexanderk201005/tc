﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcModel
{
    public class TcTaxDescription
    {
        [Key]
        public long Id { get; set; }

        [StringLength(20), Required]
        public string Description { get; set; }

        public int FiscalTaxId { get; set; }

        public virtual ICollection<TcCheckDetail> CheckDetails { get; set; }

        public TcTaxDescription()
        {
            CheckDetails = new HashSet<TcCheckDetail>();
        }
    }
}
