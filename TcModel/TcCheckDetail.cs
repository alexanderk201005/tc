﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcModel
{
    public class TcCheckDetail
    {
        [Key]
        public long Id { get; set; }

        [StringLength(50), Required]
        public string Name { get; set; }

        public int Count { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public decimal Price { get; set; }

        [ForeignKey("TcCheck")]
        public long CheckId { get; set; }
        public TcCheck TcCheck { get; set; }

        [ForeignKey("TcTaxDescription")]
        public long TaxDescriptionId { get; set; }
        public TcTaxDescription TcTaxDescription { get; set; }

        [DisplayName("Дата изменения")]
        [Column(TypeName = "datetime2")]
        public DateTime ModifiedDate { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}
